#encoding: utf-8
from django.db import models
from django.contrib.auth.models import User

class Bebida(models.Model):
    nombre = models.CharField(max_length=50)
    ingrediente = models.TextField()
    preparacion = models.TextField()
    def __unicode__(self):
        return self.nombre

class Receta(models.Model):
    #DATO CADENA, LONGITUD MAXIMA 100 Y UNICO
    titulo = models.CharField(max_length=100, unique=True)
    #DATO TEXTO, CON TEXTO DE AYUDA
    ingredientes = models.TextField(help_text='Redacta los ingredientes')
    #DATO TEXTO, CON NOMBRE: Preparacion
    preparacion = models.TextField(verbose_name='Preparación')
    #DATO IMAGEN, SE ALMACENARAN EN LA CARPETA RECETAS, TITULO: Imágen
    imagen = models.ImageField(upload_to='recetas', verbose_name='Imágen')
    #DATO FECHA Y HORA, ALMACENA LA FECHA ACTUAL
    tiempo_registro = models.DateTimeField(auto_now=True)
    #ENLACE AL MODELO USUARIO QUE DJANGO YA TIENE CONSTRUIDO
    usuario = models.ForeignKey(User)

    def __unicode__(self):
        return self.titulo
